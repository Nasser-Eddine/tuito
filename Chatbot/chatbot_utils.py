# Imports 
import pickle
import numpy as np 
from nltk import word_tokenize 
from nltk.tokenize import word_tokenize
import spacy
nlp = spacy.load('fr_core_news_md')

# get the vocabulary 
def get_vocabulary(column:iter)->np.array:
    '''
        get all the distinct vocabulary of the given corpus 
        column must be an iterable (list, set, pandas.Series, ...)
    '''
    return np.array(list({token for sentence in column for token in word_tokenize(sentence)}))

# preprocessing 
def preprocessing(text:str)->str:
    '''
        use some preprocessing method on the corpus : 
            * lowerCasing 
            * lemmatization 
    '''

    # lemmatization (include lowerCasing)
    doc = nlp(text)
    tokens_lemmatized = " ".join([token.lemma_ for token in doc])

    return tokens_lemmatized

# Bag of words 
def bow(sentence:str, vocabulary:list)->np.array:
    '''
        vocabulary      =      [a,b,c,d,e] 
        sentence =  "d e c"  : [0,0,1,1,1]
    '''
    x_bow = [] 
    for word in vocabulary:
        if word in sentence: x_bow.append(1)
        else: x_bow.append(0)
    return np.array(x_bow)


""" --------------------- Chatbot --------------------- """

# AI Answer 
def AI_response(input_message:str, vocabulary_path:str, scaler_path:str, model_path:str, label2response_path:str)->str:
    '''
        returns an answer to the input query (utterance)
    '''
    # 1. preprocessing 
    input_msg_prepro = preprocessing(input_message)
    
    # 2. import the vocabulary 
    vocabulary_file = open(vocabulary_path, "rb")
    vocabulary = pickle.load(vocabulary_file)

    # 2. vectorization : bag of word 
    x = bow(input_msg_prepro, vocabulary)
    x = x.reshape(1, x.shape[0])
    
    # 3. normalize the example 
    loaded_scaler = pickle.load(open(scaler_path, 'rb'))
    x = loaded_scaler.transform(x)

    # 3. predict the label 
    loaded_model = pickle.load(open(model_path, 'rb')) # import the model 
    label = loaded_model.predict(x)[0]

    # 4. match the label to the corresponding answer 
    #label2response_file = open(label2response_path, "rb")
    label2response = pickle.load(open(label2response_path, 'rb'))
    response = label2response[label]

    return response