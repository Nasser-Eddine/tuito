from tkinter import *
import emoji
from chatbot_utils import AI_response

def send():
    msg = EntryBox.get("1.0",'end-1c').strip()
    EntryBox.delete("0.0",END)

    if msg != '':
        ChatLog.config(state=NORMAL)
        ChatLog.insert(END, "You : " + msg + '\n')
        ChatLog.config(foreground="#000000", font=("Georgia", 12 ))

        # Wake Word 
        if msg.lower() in ["hey tuito","salut tuito","bonjour","bonsoir","hello","hi","salut"]:
            ChatLog.insert(END, "Tuito : " + "Salut ! En quoi puis je vous aider ?" + '\n\n')
            ChatLog.config(state=DISABLED)
            ChatLog.yview(END)

        # AI Response 
        else:
            vocabulary = "../Data/Data4Classification/vocabulary.pickle"
            scaler_path = "Scaler_Records/scaler_30_July_2021.sav"
            model_path = "Model_Records/model_30_July_2021.sav"
            label2response_path = "../Data/Responses/label2response.pickle"

            res = AI_response(msg, 
                            vocabulary, 
                            scaler_path,
                            model_path, 
                            label2response_path)
            
            if res == "NO_ANSWER":
                res = emoji.emojize(':thumbs_up:')
            ChatLog.insert(END, "Tuito : " + res + '\n\n')
            ChatLog.config(state=DISABLED)
            ChatLog.yview(END)

        

base = Tk()
base.title("Tuito Chatbot")
base.geometry("600x500")
base.configure(bg='#FFC300')
base.resizable(width=FALSE, height=FALSE)

# Chat window
ChatLog = Text(base, bd=0, bg="#FFC300", height="8", width="50", font="Georgia", borderwidth=10)
ChatLog.config(state=DISABLED)

# Bind scrollbar to Chat window
ScrollBar = Scrollbar(base, command=ChatLog.yview, cursor="left_ptr")
ChatLog['yscrollcommand'] = ScrollBar.set

# Send Button 
SendButton = Button(base, font=("Georgia",12,'bold'), text="Envoyer", width="12", height=5,
                    bd=0, bg="blue", command=send)

# Message Entry Box 
EntryBox = Text(base, bd=0, bg="#CECECE", width="29", height="5", font="Georgia", 
                padx=10, pady=10)

# Instanciations
ScrollBar.place(x=576, y=6, height=436)
ChatLog.place(x=6, y=6, height=436, width=560)
EntryBox.place(x=128, y=451, height=40, width=465)
SendButton.place(x=6, y=451, height=40)

base.mainloop()