# Imports 
import pandas as pd 
import numpy as np 
import pickle
from sklearn.svm import SVC 
from sklearn.utils import shuffle
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from chatbot_utils import get_vocabulary, preprocessing, bow, AI_response


# 1. import the data 
data_file = "../Data/Data4Classification/data4chatbot.csv"
df = pd.read_csv(data_file)
print(f"The data file \"{data_file}\" has been imported")


# 2. preprocessing the utterance 


# 3. get the vocabulary (all words)
all_vocab = get_vocabulary(column=df['Utterance'])
print(f"All the vocabulary is retrieved")

# 4. Export the vocabulary in the folder : Data/Data4Classification
vocabulary_file = open("../Data/Data4Classification/vocabulary.pickle","wb")
pickle.dump(all_vocab, vocabulary_file)

# 4. vectorization : bag of words 
X_bow = np.array([bow(utt, all_vocab) for utt in df['Utterance']])
print(f"Vectorization done : X_bow.shape = {X_bow.shape}")

# 5. Normalize the features 
scaler = StandardScaler().fit(X_bow)
X_bow_stand = scaler.transform(X_bow)

# 6. Export the scaler 
scaler_file = open("Scaler_Records/scaler_30_July_2021.sav","wb")
pickle.dump(scaler, scaler_file)

# 6. import the labels (intents) 
y = df['Label'].to_numpy()
print(f"y_shape = {y.shape}")

# 7. Export a dictionary of label2answer 
label2response = {}
for i in df['Label'].unique():
    label2response[i] = df[df['Label']==i]['Response'].unique()[0]

responses_file = open("../Data/Responses/label2response.pickle","wb")
pickle.dump(label2response, responses_file)

# 8.Training the model 
svm = SVC()
svm.fit(X_bow_stand, y)

# accuracy on the train set itself 
y_pred = svm.predict(X_bow_stand)
#print(y_pred.shape)
accuracy = accuracy_score(y, y_pred)
print("accuracy : ", accuracy)

# 9. save the model 
filename = 'Model_Records/model_30_July_2021.sav'
pickle.dump(svm, open(filename, 'wb'))

