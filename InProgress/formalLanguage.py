# Imports 
import re 
from tuito.grammar import conjugation, inversion_sujet


# Term types detection 
def term_type(term:str) -> str:
    ''' returns the type of the term in the sequence '''

    # [expression]
    if all(c in term for c in ['[', ']']):
        return "ONE_IN_TWO"
    # (word):{modif}
    if all(c in term for c in ['(', ')', ':', '{', '}']):
        return "MANDATORY_2_MODIFY"
    # (word)
    if all(c in term for c in ['(', ')']):
        return "MANDATORY_CHOICES"
    # word
    if all(c not in term for c in ['(', ')', ':', '{', '}', '|', ',']):
        return "MANDATORY"
    # not valid 
    else: 
        return "FORMAT_NOT_VALID"

############################# MODIFICATION (term types) ###########################
# ONE_IN_TWO 
def one_in_two(term:str) -> list: 
    '''
        return a list that contains the term in addidtion to the empty string 
        that corresponds to the time where the term will not be used. 
    '''
    terms = ['',]
    term = term.replace('[','').replace(']','')
    
    # MANDATORY_2_MODIFY
    if "}" in term: 
        terms.extend(mandatory_2_modify(term))
    
    # MANDATORY_CHOICES
    elif "(" in term: 
        terms.extend(mandatory_choices(term))
    
    else: 
        terms.append(term)
    
    return terms

# MANDATORY_CHOICES
def mandatory_choices(term:str) -> list: 
    '''

    '''
    # remove '(' ')'
    term = term.replace('(','').replace(')','')

    # in case there is many choices (which is supposed to be the case)
    if '|' in term:
        w_list = []
        return [t for t in term.split('|')]
    
    # in case there is one single choice 
    else: 
        return [term]

# MANDATORY_2_MODIFY
def mandatory_2_modify(term:str) -> list:
    '''

    '''   
    # verify the format 
    if term.count("(") == 2 and term.count(")") == 2:
        pronouns = re.findall("\((.*?)\)", term)[0]     # "p1|p2|p3"
        verbs = re.findall("\((.*?)\)", term)[1]        # "v1|v2|v3"
        tenses = re.search("\{(.*?)\}", term).group(1)  # "t1,t2,t3"

        verb2tense = []
        for pronoun in pronouns.split('|'):
            for verb in verbs.split("|"):
                for tense in tenses.split(","):
                    
                    # case : inversion du sujet 
                    if "_inversion_" in term:
                        verb2tense.append(inversion_sujet(pronoun, verb, tense))
                    else:
                        verb2tense.append(conjugation(pronoun, verb, tense))
        
        return verb2tense
    
    # format invalid 
    else: 
        print("the format of pronoun-verb-tense is not valid")
        print("It is supposed to be (pronoun)(verb):{tense}")


    
        
        
############################# Expression Generation ##############################


def variables_detection(variables:str)->dict:
    '''
    
    '''
    variable_dict = {}
    for v in re.findall("\$.*?\]", variables):
        variable_dict[v] = 


##### Pattre.findall("\$.*?\]", T)erns 
def no_pattern(action:str)->str:
    ''''''
    