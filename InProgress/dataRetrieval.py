import pandas 


# Gather Excel Sheets into one dataframe 
def sheets2df(sheets:dict) -> pandas.DataFrame:
    '''Gathers excel sheets into on single dataframe and adds a column for sheet's name'''

    data = pandas.DataFrame()
    for name, sheet in sheets.items():
        # if the sheet is a context (contain "Exp")
        if "Exp" in name:
            sheet['Exp'] = pandas.DataFrame([name]*sheet.shape[0])
            data = pandas.concat([data, sheet])
    return data[['Utterance', 'UtteranceCode', 'UtteranceType', 'Exp',
                    'FormalGrammar', 'Var', 'Ideas', 
                    'Situation', 'SituationCode', 
                    'Response', 'Prompt']]