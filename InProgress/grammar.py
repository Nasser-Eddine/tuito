from verbecc import Conjugator



# Conjugation 
cg = Conjugator(lang='fr')
def conjugation(pronoun, verb, tense):
    '''
        
    '''
    if tense == "présent": mood, tense = ("indicatif","présent")
    elif tense == "conditionnel": mood, tense = ("conditionnel","présent")
    elif tense == "impératif": mood, tense = ("imperatif","imperatif-présent")
    elif tense == "futur": mood, tense = ("indicatif","futur-simple")
    else: return "the tense is not valid"
    
    # define the pronoun
    pronouns = {"je":0,"tu":1,"il":2,"elle":2,"on":2,
              "nous":3,"vous":4,"ils":5,"elles":5}
    pronouns_imperatif = {"tu":0,"nous":1,"vous":2}
    
    if tense == "imperatif-présent":
        pronoun = pronouns_imperatif[pronoun]
    else: 
        pronoun = pronouns[pronoun]

    # return pronoun + conjugated verb 
    return cg.conjugate(verb)["moods"][mood][tense][pronoun]


def inversion_sujet(pronoun:str, verb:str, tense:str)->str:
    '''
    
    '''
    conjugated_verb = conjugation(pronoun, verb, tense).split()[1]
    if verb == "pouvoir" and tense == "présent" and pronoun == "je":
        return "puis je"
    else: 
        return conjugated_verb + " " + pronoun 
