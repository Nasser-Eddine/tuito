from itertools import combinations, permutations
#from deep_translator import GoogleTranslator
import pandas as pd
from tqdm import tqdm
import spacy




# Utterance Permutations 

def utterance_twist(utterance):
    '''
        from an utterance, generate a list of new utterance by extracting n-grams and twisting them 
    '''

def utt2combs(utterance):
    '''
    
    '''

    tokens = utterance.split()
    combs = []    
    
    for i in range(1,len(tokens)+1):
        C = list(combinations(tokens, i))
        #print(C, end="\n\n")
        for c in C: 
            c = list(c)
            P = list(permutations(c)) 
            combs.extend([" ".join(list(p)) for p in P])

    return combs

def permutation2label(s, gt, command, radical, obj):
    '''
        S : sequences 
        gt is the ground truth 
        _G for Grey zone
        keywords : list of important tokens 
    '''

    # if the sequence is empty 
    if s.strip() == "": 
        return "empty sequence"

    # remove the spaces in the ground truth gt 
    gt = " ".join([g.strip() for g in gt.split()])

    # when the sequence contains one single token 
    if len(s.split()) == 1: 
        return "no_command"

    # when the sequence contains more than one token
    elif len(s.split()) > 1: 
        # respects the order (radical then object)
        if radical + " " + obj in s: 
            return command
        
        # if radical and object are in the sequence 
        elif radical in s and obj in s: 
            return command + "_G"

        # no command 
        else: 
            return "no_command"


# Translation 
def new_utterances(utterance, languages):
    '''



        languages = {"English":"en", 
                     "Arabic" :"ar",
                     "Swedish":"sw",
                     "Russian":"ru",
                     "Spanish":"es",
                     "German" :"de"}
    
    '''
    #new_utterances = {}
    #for l, l_code in languages.items():
    #    translator = GoogleTranslator(source='auto', target=l_code).translate(utterance)
    #    translation_back = GoogleTranslator(source='auto', target='fr').translate(translator)
    #    new_utterances[l] = translation_back
    #return new_utterances
    pass 

def newUt(data, language):
    '''
        return a pd.DataFrame of previous and new utterances and their labels 
        data : pd.DataFrame of utterances and their labels  
    '''
    # new_data = []
    # nbr_NU = 0 # number of new uterrances 
    # # gt, new/old, command, language
    # for index, row in data.iterrows():
    #     groundTruth = row['GT_Preprocessed']
    #     command = row['Command']
    #     # generate new uterrance 
    #     translation = GoogleTranslator(source='auto', target=language).translate(groundTruth)
    #     translation_back = GoogleTranslator(source='auto', target='fr').translate(translation)
    #     # add the groundTruth uterrance to new_data
    #     new_data.append({'GroundTruth':groundTruth,
    #                      'Type':'standard',
    #                      'Command':command,
    #                      'Language':language})
    #     # verify if it is different that the origin uterrance 
    #     if translation_back != groundTruth: 
    #         new_data.append({'GroundTruth':translation_back,
    #                          'Type':'new',
    #                          'Command':command,
    #                          'Language':language})
    #         nbr_NU += 1 
    # print(f"<{language}> : {nbr_NU} new uterrances have been generated")
    # return pd.DataFrame(new_data)
    pass 




    
# conjugation 
from verbecc import Conjugator
cg = Conjugator(lang='fr')
def conjugation(verb, tense, person):
    '''
        
    '''
    if tense == "présent": mood, tense = ("indicatif","présent")
    elif tense == "conditionnel": mood, tense = ("conditionnel","présent")
    elif tense == "impératif": mood, tense = ("imperatif","imperatif-présent")
    elif tense == "futur": mood, tense = ("indicatif","futur-simple")
    else: return "the tense is not valid"
    return cg.conjugate(verb)["moods"][mood][tense][person]

# many conjugation 
def conjug_group(verb2tenses):
    '''
        verb2tenses = {
                "Verb1":{"tenses":["tense1","tense2"],"persons":["Je","Tu"]},
                "Verb2":{"tenses":["tense1","tense2"],"persons":["Je","Tu"]},
                ... 
        }
        
    '''
    conjugations = []
    for verb, dic in verb2tenses.items():
        for tense, person in zip(dic['tenses'], dic['persons']):
            conjugations.append(conjugation(verb, tense, person))
    return conjugations

# inversion du sujet 
def subject_invert(subj_verb): 
    '''
    
    '''
    s, v = subj_verb[0], subj_verb[1]
    if s.lower() == "je" and v == "peux": 
        return "puis-je"
    return v + "-" + s

def subject_invert_group(subj_verb_list):
    '''
    
    '''
    inverted = []
    for sv in subj_verb_list: 
        inverted.append(subject_invert(sv))
    return inverted
    
    

def pouvoir(action:str, obj:str, command:str, class_:str) -> list:
    '''
    
    '''
    new_utt = []
    for ask in ["","est ce que "]:
        for person in [1,4]:
            for tense in ["présent","conditionnel"]:
                v = str(conjugation("pouvoir", tense, person))
                u = ask + v + " " + action + " " + obj
                new_utt.append({"Utterance": u,"Class":class_, "Command":command, "Grammatical_Type":"pouvoir_"+tense+"_"+ask})
                
    return new_utt
    
def want_smtg(command:str, class_:str, obj:str, action:str="") -> list:
    '''
    
        # je souhaite action obj 
        # je veux action obj 
        
        # je souhaiterai action obj 
        # je voudrais action obj 
        # j'aimerai action obj 
        
        # j'ai besoin de action obj 
        # j'aurai besoin de action obj 
    '''
    
    new_utt = []
    verbs = {"souhaiter":["présent","conditionnel"],
             "vouloir":["présent","conditionnel"],
             "aimer":["conditionnel"]}
    
    for verb, tenses in verbs.items():
        for tense in tenses:
            v = str(conjugation(verb, tense, person=0))
            u = v + " " + action + " " + obj
            new_utt.append({"Utterance": u,"Class":class_, "Command":command, "Grammatical_Type":verb + "_" + tense})
            
    # j'ai besoin de obj 
    # j'aurai besoin de obj 
    for aux in ["ai","aurais"]:
        if action[0] in ['a','e','u','i','o','y','é']:
            u = f"j'{aux} besoin d'{action} {obj}"
        else:
            u = f"j'{aux} besoin de {action} {obj}"
        
        new_utt.append({"Utterance": u,"Class":class_, "Command":command, "Grammatical_Type":"avoir_besoin"})
    
    return new_utt
  
def imperatif(action:str, obj:str, command:str, class_:str) -> list:
    '''

    '''
    new_utt = []
    for p in [0,2]:
        v = conjugation(verb=action, tense="impératif", person=p)
        u = v + " " + obj
        new_utt.append({"Utterance": u,"Class":class_, "Command": command, "Grammatical_Type":"impératif"})
    return new_utt

def no_change(action:str, obj:str, command:str, class_:str) -> dict:
    '''

    '''
    u = action + " " + obj
    return {"Utterance": u,"Class":class_, "Command": command, "Grammatical_Type":"no_change"}

def qs_inversion_sujet(action:str, obj:str, command:str, class_:str, persons:list=[1,4]) -> list:
    '''
        
        
        persons : [je:0, tu:1, il:2, nous:3, vous:4, ils:5]
        
        
        Example : qs_inversion_sujet(action="boire", 
                                     obj="un verre d'eau", 
                                     command="UTT_CALL_WATER", 
                                     class_="UTT_CALL", 
                                     persons=[0])
            
    '''
    new_utt = []
    for t in ["présent","conditionnel"]:
        for p in persons:
            v = conjugation(verb="pouvoir", tense=t, person=p)
            person, verb = v.split()[0], v.split()[1]
            # special case : "peux-je" --> "puis-je"
            if person == "je": 
                verb = "puis"
            u = verb + " " + person + " " + action + " " + obj 
            new_utt.append({"Utterance": u,"Class":class_, "Command": command, "Grammatical_Type":"qs_inversion"})
        
    return new_utt
    