What is MIVOCO Excel ? 
    MIVOCO is an excel file divided into several sheets that contains the utterances of the MIVOCO 
    project. 

What do Exp sheets refer to ? 
    They contain features related to the utterances. 

What do each feature refer to ? 
    * Utterance : 
        - The utterance is the transcription of the vocal command said by the user. 
        - Should not contain "/", ",", ";" or other symbols to separate tokens. Please use FormalGrammar 
        and Var columns in order to add other possibilities. 
        - Gather the utterances that can be !
            Counterexemple : "Faire un réglage du lit" and "Régler le lit" can not be gathered. 
        - Use the infinitive form as much as possible. 
            Example : "règle le lit" => "régler le lit"  
        - Do not use "demand expression" (this can be written in the Ideas column): 
            Example : "je souhaite effectuer un réglage" => "effectuer un réglage"
        - Group utterance cells by their UtteranceCode so that they are consecutively presented.

    * UtteranceCode : 
        - UtteranceCode corresponds to the utterance intent. This utterance should represent as much as 
        possible the meaning of the utterance. 
        - Should not include different meanings. For instance, "augmenter/diminuer le volume" should 
        not correspond to the same utteranceCode. 
        - To suggest new UtteranceCodes, start with "UTT_". For example : "UTT_DO_SETTING_BED"

    * UtteranceType : 
        - "WW" : Wake Word
        - "Principal" : Principal utterances used for classification models, 
        - "Context" : Context utterances that come after the vocal assistance questions and that is not 
        a Principal utterance. 

    * FormalGrammar : 
        The rule, corresponding to the utterance, that will enable new utterances generation. 
        Example : 
            FormalGrammar : 
                $V $A lit à $X centimètres 
            Var : 
                $V = [régler, mettre]
                $A = [le, mon, son] 
                $X=INT[50…100]

    * Var : 
        Variables used in the Formal Grammar. It includes synonyms of verbs and nouns, articles, 
        integers and floats, general variables for expressions. 
        A specific notation could be used (V:verb, N:noun, A:article, E:expressions, G:general, 
        I:integer, F:float, ...) but it's not mandatory. 

    * Ideas : 
        This column is used to mark down some ideas, remarks, special cases, etc... 

    * Situation : 
        The situation text describe the context of each utterance. 

    * SituationCode : 
        The SituationCode corresponds to the situation. 

    * Response : 
        Response is the answer said by the vocal assistant via the Text To Speech (TTS) process.
        It corresponds to the situation that itself is related to the utterance.

    * Prompt : 
        ??? 



